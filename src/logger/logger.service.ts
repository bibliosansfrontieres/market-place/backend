import { ConsoleLogger } from '@nestjs/common';

export class LoggerService extends ConsoleLogger {
  error(message: any, context?: string) {
    super.error(message, context);
  }

  log(message: any, context?: string) {
    super.log(message, context);
  }

  warn(message: any, context?: string) {
    super.warn(message, context);
  }
}
