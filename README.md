# Marketplace backend

## Use for development

### With Docker

Create a file named .env.development in the root of the project with the following content:

```
# EMPTY FOR NOW
```

Then run the following command:

```
docker-compose up -d
```

### With NodeJS

Create a file named .env.development in the root of the project with the following content:

```
# EMPTY FOR NOW
```

Then run the following command:

```
yarn run start:dev
```
